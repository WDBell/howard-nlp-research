from datetime import datetime, timedelta
import time
import sys
import argparse

"""
Function will return if a test_time is in the forward range of an initial
timestamp.
Parameters:
    start: the initial timestamp and begining of the acceptable range
           datetime.datetime
    range: the length of the forward range acceptable
           datetime.timedelta
    test_time: the time that will be tested for whether it is in range or not
                datetime.datetime
Returns:
    Bool: True if test_time is in range, False if not.
"""
def inRange(start, range, test_time):
    return test_time - start  < range


"""
Function will extract tweets that fall into a hardcoded range.
Parameter:
    file: Name of file that contains formatted tweets to be iterated through
Returns:
    inRangeTweets: List of extracted tweets represented in strings.
"""
def extract_tweets(file):
    inRangeTweets = []
    format = '%Y-%m-%d %H:%M:%S'
    fin = open(file, "r")
    for line in fin:
        tweet_time = line[22:52]
        timestamp = time.strftime(format, time.strptime(tweet_time,'%a %b %d %H:%M:%S +0000 %Y'))
        ts = datetime.strptime(timestamp, format)
        if inRange(datetime(2016, 9, 19, 2, 17, 11), timedelta(minutes = 5), ts):
            inRangeTweets.append(line)
    return inRangeTweets



def main(file):
    extractedTweets = extract_tweets(file)

if __name__ == "__main__":
    print("Note: The code extracts tweets from given range and stores them in list of strings.")

    # Parse and setup flag, args.file
    parser = argparse.ArgumentParser()
    parser.add_argument("--file", help="Required Flag: Tweet text File.")
    args = parser.parse_args()
    # If the flag is not passed
    if not args.file:
        print("Please pass a file containing text tweets.")
        print("Using the following command line syntax.")
        print("python3 task1 --file=samplefilename.txt")
        sys.exit(-1)

    file = args.file
    main(file)
    print("SUCCESS")

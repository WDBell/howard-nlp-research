import task1
import re
from nltk.util import ngrams
import nltk
from collections import Counter


#path to my dataset of tweets
path = 'Sample labelled dataset.txt'
tweets = task1.extract_tweets(path)


#path to my wikipedia file
wiki = 'complete_wikipedia_backlinks.txt'

backlinks=[]

#Store wikipedia file in list
with open(wiki) as myfile:
    for line in myfile:
        backlinks.append(line.lower().strip('\t').strip('\n'))

backlinks_str = ''.join(backlinks)
backlinks_new = re.split(r'\t+', backlinks_str)

unigram_str=[] #unigram list i.e. single word
bigram_str=[]  #bigram list i.e. double words
trigram_str=[] #trigram list i.e. three words

for y in range(len(tweets)):
    tokens = nltk.word_tokenize(tweets[y]) #identifies single words
    unigram = ngrams(tokens,1) #selects only single words e.g. kanye
    bigram = ngrams(tokens,2)  #selects only double words e.g. kanye west
    trigram = ngrams(tokens,2) #selects only three words e.g. ll cool j
    for a in unigram:
        unigram_str.append(' '.join(a))
    for b in bigram:
        bigram_str.append(' '.join(b))
    for c in trigram:
        trigram_str.append(' '.join(c))

unigram_entities=[]
bigram_entities=[]
trigram_entities=[]

print("Unigram length: ", len(unigram_str))
print("Bigram length: ", len(bigram_str))
print("Trigram length: ", len(trigram_str))
print("Backlinks lenght: ", len(backlinks_new))


for i in unigram_str:
    if i in backlinks_new:
        unigram_entities.append(i)
print("Unigrams found")

for j in bigram_str:
    if j in backlinks_new:
        bigram_entities.append(j)
print("Bigrams found")

for k in trigram_entities:
    if k in backlinks_new:
        trigram_entities.append(k)
print("Trigrams found")

unigram_occ = Counter(unigram_entities)
bigram_occ = Counter(bigram_entities)
trigram_occ = Counter(trigram_entities)

print(list(set(unigram_entities)))
print(list(set(bigram_entities)))
print(list(set(trigram_entities)))
